<?php

/**
 * Implements hook_field_widget_info().
 */
function referenceit_field_widget_info() {
  return array(
    'referenceit' => array(
      'label' => t('ReferenceIt'),
      'field types' => array('node_reference'),
      'settings' => array(
        'size' => 60,
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Old Implements hook_field_widget_form().
 */
function _referenceit_field_widget_form(&$form, &$form_state, $field, $instance,
    $langcode, $items, $delta, $element) {
  // Add a Create button the field.
  
  $element += array(
    '#type' => 'textfield',
    '#default_value' => isset($items[$delta]['nid']) ? $items[$delta]['nid'] : NULL,
    '#size' => $instance['widget']['settings']['size'],
    '#element_validate' => array('node_reference_autocomplete_validate'),
    '#value_callback' => 'node_reference_autocomplete_value',
    '#ajax' => array(
      'event' => 'search',
      'callback' => '_referenceit_autocomplete',
      'progress' => array('type' => 'none'),
    ),
    '#attributes' => array(
      'class' => array('data-container'),
    ),
  );

  $default_value = NULL;
  if ($element['#default_value'] !=  NULL) {
    $node = node_load($element['#default_value']);
    $default_value = $node->title;
  }

  $textfield = array(
    '#type' => 'textfield',
    '#title' => $instance['label'],
    '#description' => $instance['description'],
    '#default_value' => $default_value,
    '#attributes' => array(
      'autocomplete' => 'off',
      // @todo Change the placeholder text dependant on what one can search for.
      'placeholder' => 'Search for content',
      'class' => array(
        'dropdown-select',
        isset($default_value) ? 'connected' : '',
      ),
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'referenceit') . '/js/referenceit.js',
      ),
      'css' => array(
        drupal_get_path('module', 'referenceit') . '/css/referenceit.css',
      ),
    ),
  );
  
  $create = array(
    '#type' => 'button',
    '#value' => t('Create'),
    '#attributes' => array(
      'class' => array('create-modal', 'controls'),
    ),
    '#ajax' => array(
      'callback' => 'referenceit_entity_create_modal',
      'progress' => array('type' => 'none'),
    ),
  );
  
  $browse = array(
    '#type' => 'button',
    '#value' => t('Browse'),
    '#attributes' => array(
      'class' => array('browse-modal', 'controls'),
    ),
    '#ajax' => array(
      'callback' => 'referenceit_entity_browse_modal',
      'progress' => array('type' => 'none'),
    ),
  );
  
  return array(
    'nid' => $element,
    'textfield' => $textfield,
    'create' => $create,
    'browse' => $browse,
    'wrapper' => array(
      '#prefix' => '<div class="dropdown">',
      '#suffix' => '</div>',
    ),
    'messages' => array(
      '#prefix' => '<div id="messages">',
      '#suffix' => '</div>',
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function referenceit_field_widget_form(&$form, &$form_state, $field, $instance,
    $langcode, $items, $delta, $element) {
  
  $element += array(
    '#type' => 'referenceit',
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'referenceit') . '/js/core.js',
      ),
    ),
  );
  $element['#title'] = $instance['label'];
  $element['#description'] = $instance['description'];
  
  return array(
    'widget' => $element,
  );
}