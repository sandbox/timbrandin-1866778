<?php
?>
<div class="referenceit">
  <input class="listener"/>
  <div class="controls">
    <button class="create">Create</button>
    <button class="browse">Browse</button>
  </div>
  <div class="input">
    <div class="operations">
      <button class="edit"></button>
      <button class="remove"></button>
    </div>
    <input value="" />
  </div>
  <div class="autocomplete">
    <ul>
      <li></li>
      <li class="add"></li>
      <li class="browse">
        <p class="title"></p>
        <p class="detail"></p>
      </li>
    </ul>
  </div>
</div>