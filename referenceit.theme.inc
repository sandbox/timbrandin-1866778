<?php

/**
 * Adding the styling for the element.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 */
function theme_referenceit_widget($variables) {
  drupal_add_css(drupal_get_path('module', 'referenceit') . '/css/base.css', 
          array('group' => CSS_DEFAULT, 'every_page' => FALSE));
}