(function ($) {

  Drupal.behaviors.referenceitDropDown = {
    attach: function (context, settings) {
      this.bindings();
    },
    bindings: function() {
      var self = this;
      var current_value;
      var $responses = $('.autocomplete-wrapper li');
      $responses.hover(function() {
        $responses.removeClass('selected');
        $(this).addClass('selected');
      });
      $responses.mousedown(function() {
        self.select($(this).closest('.autocomplete-wrapper').prev());
      });

      $('input.dropdown-select').once('referenceitDropDown', function(index, item) {
        var $selected;
        var $item = $(item);
        var $listener = $('input', $item.parent('.form-item').prev());
        var $wrapper = $('<div />', {'class': 'autocomplete-wrapper'});
        $item.after($wrapper);
        // Set width of wrapper so it matches the searchfield.
        $wrapper.css({width: $item.outerWidth()-2});

        // Timeout variable to stop any ongoing event/request.
        var t;

        $item.bind('keydown', function(e) {

          switch (e.keyCode) {
            // ESC clears the field.
            case 8:
              if ($item.val().length == 0) {
                e.preventDefault();
              }
              break;
            // ESC clears the field.
            case 27:
              $item.val(''); //.blur();
              break;
            // UP
            case 38: 
              e.preventDefault();
              break;
            // DOWN
            case 40: 
              e.preventDefault();
              break;
          }
        });

        $item.bind('keyup', function(e) {

          switch (e.keyCode) {
            // ENTER
            case 13:
              self.select($item);
              break;
            // UP
            case 38: 
              e.preventDefault();
              $selected = $('li.selected', $wrapper);
              // Select upwards in the response array
              if ($selected.prev('li').length > 0 ) {
                $selected.removeClass('selected');
                $selected.prev('li').addClass('selected');
              }
              break;
            // DOWN
            case 40: 
              e.preventDefault();
              $selected = $('li.selected', $wrapper);
              // Select downwards the in response array
              if ($selected.next('li').length > 0 ) {
                $selected.removeClass('selected');
                $selected.next('li').addClass('selected');
              }
              break;
            default:
              $wrapper.hide();
              $item.trigger('search');
              break;
          }
        });

        $item.bind('search', function() {
          clearTimeout(t);
          if ($item.val().length > 0) {
            t = setTimeout(function(){
              $listener.val($item.val());
              $listener.trigger('search');
            }, 50);
          }
          else {
            $wrapper.hide();
          }
        })

        $item.bind('mouseup', function() {
          $item.trigger('search');
          current_value = $listener.val();
          if (!$item.hasClass('focused')) {
            $(this).select();
            $item.addClass('focused');
          }
        });

        $listener.bind('autocomplete', function(e, data) {
          if ($item.is(':focus')) {
            $wrapper.html('<ul>' + data + '</ul>');
            $wrapper.show();
          }
        });

        $item.bind('blur', function() {
          clearTimeout(t);
          $wrapper.hide();
          $item.removeClass('focused');
          if ($item.val().length > 0) {
            $item.addClass('connected');
          }
          else {
            $item.removeClass('connected');
            current_value = '';
          }
          if (!/\[nid:\d+\]/.test($listener.val())) {
            $item.val(current_value.replace(/\[nid:\d+\]/, '').trim());
            $listener.val(current_value);
          }
        });
      });
    },
    select: function($item) {
      var $wrapper = $item.next('.autocomplete-wrapper');
      var $selected = $('li.selected', $wrapper);
      // Set the value of the field.
      if ($selected.hasClass('browse')) {
        // @todo: Open browser window and search for the term used.
        $item.val('').blur();
        return;
      }
      else if ($selected.hasClass('create')) {
        // @todo: Open create window and prefill the title.
        $item.val('').blur();
        return;
      }
      else {
        this.connect($selected);
        $item.val($selected.text()).blur();
      }
    },
    connect: function($option) {
      var id = $option.attr('data-id');
      var data_container = $('input.data-container', $option.closest('.form-wrapper'));
      data_container.val(id);
    }
  };

}(jQuery));
