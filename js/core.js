(function ($) {

  Drupal.behaviors.referenceit = {
    attach: function(context, settings) {
      this.elements();
      this.bind();
    },
    /* Locate all elements for this instance. */
    elements: function() {
      this.listener = $('.referenceit .listener');
      this.input = $('.referenceit .input .form-type-textfield input');
      this.autocomplete = $('.referenceit .autocomplete');
      this.autocomplete.css({width: this.input.outerWidth()-2});
      this.selected;
      this.timeout;
      this.current_value = '';
    },
    bind: function() {
      this.bindEvents();
      this.bindKeyEvents();
    },
    /* Bind to events for elements in this instance. */
    bindEvents: function() {
      var self = this;
      self.input.bind('search', function() {
        clearTimeout(self.timeout);
        if (self.input.val().length > 0) {
          self.timeout = setTimeout(function(){
            self.listener.val(self.input.val());
            self.listener.trigger('search');
          }, 50);
        }
        else {
          self.autocomplete.hide();
        }
      })

      self.input.bind('mouseup', function() {
        self.input.trigger('search');
        self.current_value = self.listener.val();
        if (!self.input.hasClass('focused')) {
          $(this).select();
          self.input.addClass('focused');
        }
      });

      self.listener.bind('autocomplete', function(e, data) {
        if (self.input.is(':focus')) {
          self.autocomplete.html('<ul>' + data + '</ul>');
          self.autocomplete.show();
        }
      });

      self.input.bind('blur', function() {
        clearTimeout(self.timeout);
        self.autocomplete.hide();
        self.input.removeClass('focused');
        if (self.input.val().length > 0) {
          self.input.addClass('connected');
        }
        else {
          self.input.removeClass('connected');
          self.current_value = '';
        }
        if (!/\[nid:\d+\]/.test(self.listener.val())) {
          self.input.val(self.current_value.replace(/\[nid:\d+\]/, '').trim());
          self.listener.val(self.current_value);
        }
      });
    },
    /* Bind to keyboard events for input elements in this instance. */
    bindKeyEvents: function() {
      var self = this;
      self.input.bind('keydown', function(e) {
        switch (e.keyCode) {
          // ESC clears the field.
          case 8:
            if (self.input.val().length == 0) {
              e.preventDefault();
            }
            break;
          // ESC clears the field.
          case 27:
            self.input.val(''); //.blur();
            break;
          // UP
          case 38: 
            e.preventDefault();
            break;
          // DOWN
          case 40: 
            e.preventDefault();
            break;
        }
      });

      self.input.bind('keyup', function(e) {
        switch (e.keyCode) {
          // ENTER
          case 13:
            self.select(self.input);
            break;
          // UP
          case 38: 
            e.preventDefault();
            self.selected = $('li.selected', self.autocomplete);
            // Select upwards in the response array
            if (self.selected.prev('li').length > 0 ) {
              self.selected.removeClass('selected');
              self.selected.prev('li').addClass('selected');
            }
            break;
          // DOWN
          case 40: 
            e.preventDefault();
            self.selected = $('li.selected', self.autocomplete);
            // Select downwards the in response array
            if (self.selected.next('li').length > 0 ) {
              self.selected.removeClass('selected');
              self.selected.next('li').addClass('selected');
            }
            break;
          default:
            self.autocomplete.hide();
            self.input.trigger('search');
            break;
        }
      });
    },
    recieve: function(e, data) {
      
    },
//    focus: function() {
//      
//    },
    search: function() {
      
    },
    cancel: function() {
      
    },
    blur: function() {
      
    },
    select: function() {
      
    },
    connect: function() {
      
    }
  };

}(jQuery));